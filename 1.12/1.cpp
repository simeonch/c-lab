#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>
#include <algorithm>

using namespace std;

class Employee
{
public:
	//constructors
	Employee()
	{
		firstName = "-";
		lastName = "-";
		hourlyWage = 0.0;
	}

	Employee(const string &firstName_, const string &lastName_, double hourlyWage_)
	{
		firstName = firstName_;
		lastName = lastName_;
		hourlyWage = hourlyWage_;
	}

	//accessors

	const string & getFirstName() const
	{
		return firstName;
	}

	const string & getLastName() const
	{
		return lastName;
	}

	const double getHourlyWage() const
	{
		return hourlyWage;
	}

	//other

	const double getMonthlyWage() const
	{
		return hourlyWage * 20 * 8;
	}

	void printRow(int nameColumnWidth, int wageColumnWidth)
	{
		cout << setw(nameColumnWidth) << lastName << setw(nameColumnWidth) << firstName << setw(wageColumnWidth) << getMonthlyWage() << endl;
	}



private:
	string firstName;
	string lastName;
	double hourlyWage;
};

int main(int argc, char * argv[])
{
	string fname, lname;
	double hourlyWage;

	Employee topEmployee;
	double total = 0;

	while (cin >> fname >> lname >> hourlyWage)
	{
		Employee empl(fname, lname, hourlyWage);

		total += empl.getMonthlyWage();

		if (topEmployee.getHourlyWage() < empl.getHourlyWage())
			topEmployee = empl;

		empl.printRow(20, 10);
		cout << setfill('-') << setw(50) << "" << setfill(' ') << endl;
	}

	cout << setw(50) << total << endl;

	cout << "Top employee is " << topEmployee.getFirstName() << " earning $" << topEmployee.getHourlyWage() << " per hour." << endl;


	return 0;
}
