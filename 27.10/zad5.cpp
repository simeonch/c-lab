#include <iostream>

using namespace std;

int main()
{
    string name, type;
    cout << "Enter a card: " << endl;
    getline(cin, name);
    cout << "Enter suit: " << endl;
    getline(cin, type);
    if(name=="2")
    {cout <<"Two";}
    else if(name=="3")
    {cout <<"Three";}
    else if(name=="4")
    {cout <<"Four";}
    else if(name=="5")
    {cout <<"Five";}
    else if(name=="6")
    {cout <<"Six";}
    else if(name=="7")
    {cout <<"Seven";}
    else if(name=="8")
    {cout <<"Eight";}
    else if(name=="9")
    {cout <<"Nine";}
    else if(name=="A")
    {cout <<"Ace";}
    else if(name=="J")
    {cout <<"Jack";}
    else if(name=="Q")
    {cout <<"Queen";}
    else if(name=="K")
    {cout <<"King";}
    cout <<" of " ;
    if(type=="d")
    {cout << "Diamonds";}
    else if(type=="h")
    {cout << "Hearts";}
    else if(type=="s")
    {cout << "Spades";}
    else if(type=="c")
    {cout << "Clubs";}
    cout << endl;
    return 0;
}
