#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int a;
    cout << "Enter a number: (to check if even or odd) " << endl;
    cin >> a;
    if (a%2==0)
    {
        cout << "The number " << a << " is even." << endl;
        return 1;
    }
    cout << "The number " << a << " is odd." << endl;
    return 0;
}
