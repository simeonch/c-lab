#include <iostream>

using namespace std;

int main()
{
	string input;
	cin >> input;
	if(input.length() < 2)
		return -1;
	char card = input[0],
		suit = input[1];
		
	if(card == 'A')
		cout << "Ace";
	else if(card == 'J')
		cout << "Jack";
	else if(card == 'Q')
		cout << "Queen";
	else if(card == 'K')
		cout << "King";
	else if(card == '2')
		cout << "Two";
	else if(card == '3')
		cout << "Three";
	else if(card == '4')
		cout << "Four";
	else if(card == '5')
		cout << "Five";
	else if(card == '6')
		cout << "Six";
	else if(card == '7')
		cout << "Seven";
	else if(card == '8')
		cout << "Eight";
	else if(card == '9')
		cout << "Nine";
		
	cout << " of ";
	
	
	if(suit == 'D')
		cout << "Diamonds";
	else if(suit == 'H')
		cout << "Hearts";
	else if(suit == 'S')
		cout << "Spades";
	else if(suit == 'C')
		cout << "Clubs";
		
	cout << endl;
	return 0;
}