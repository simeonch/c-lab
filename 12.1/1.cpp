#include <iostream>
#include <vector>
#include <string>
#include <cassert>

using namespace std;

vector<double> elm;

double sum(vector<double>  const &d)
{
    double res = 0;
    for(int i = 0; i < d.size(); i++)
        res += d[i];
    return res;
}

double avg(vector<double>  const &d)
{
    double s = sum(d);
    return s / d.size();
}

double min(vector<double> const &d)
{
    double m = d[0];
    for(int i = 0; i < d.size(); i++)
        if(m > d[i])
        m = d[i];
    return m;
}

double max(vector<double> const &d)
{
    double m = d[0];
    for(int i = 0; i < d.size(); i++)
        if(m < d[i])
        m = d[i];
    return m;
}


int main()
{
    int n;
    string comm;
    cin >> n;
    for(int i = 0; i < n; i++)
    {
        double el;
        cin >> el;
        elm.push_back(el);
    }

    while(cin >> comm)
    {
        if(comm == "ins")
        {
            double d;
            cin >> d;
            elm.push_back(d);
        }
        if(comm == "del")
        {
            int k;
            cin >> k;
            //assert(k < 0 || k > elm.size() - 1, "Invalid index: ")
            if(k < 0 || k > elm.size()-1)
            {
                cout << "Invalid index d = " << k << endl;
                continue;
            }
            elm.erase(elm.begin() + k);
        }
        if(comm == "sum")
        cout << sum(elm) << endl;
        if(comm == "avg")
        cout << avg(elm) << endl;
        if(comm == "min")
        cout << min(elm) << endl;
        if(comm == "max")
        cout << max(elm) << endl;
    }
    return 0;
}
