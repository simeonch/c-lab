/***
FN:F79618
PID:2
GID:1
*/

#include <iostream>
#include <string>
using namespace std;

string input;

void mod2(string& s)
{
    string res;
    for (int i = s.size() - 1; i >= 0; i--)
    res += s[i];
    s = res;
}

int main()
{
    while(true)
    {
    cin >> input;

    if(input=="exit")
        break;
    mod2(input);
    cout << input << endl;
    }

	return 0;
}
