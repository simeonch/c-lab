/***
FN:F79618
PID:3
GID:1
*/

#include <iostream>

using namespace std;

bool isOk(string s)
{
    for(int i = 0; i < s.size(); i++)
    {
        int d = s[i] - '0';
        if(d % 2 != 0)
            return false;
    }
    return true;
}
int main()
{
    string input;

    while(true)
    {
        cin >> input;
        if(!cin.good())
            break;
            if(isOK(input))
                cout << "YES" << endl;
            else
                cout << "NO" << endl;
            cout << input << endl;
    }
	return 0;
}
