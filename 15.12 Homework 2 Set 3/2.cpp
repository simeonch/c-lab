/***
FN:F79618
PID:2
GID:3
*/

#include <iostream>
#include <iomanip>
using namespace std;

int nmb;
double res;
double func(int s)
{
    double sum = 0;
    int a = 0;
    int n = 0;
    while(nmb > 0)
    {
        n = nmb%10;
        sum += n;
        a++;
        nmb /= 10;
    }
        res = sum/a;
        return res;
}

int main()
{
    cout << "Enter a number (integer): " << endl;
    cin >> nmb;
    double res2 = func(nmb);
    cout << setprecision(2) << fixed << res2 << endl;
    return 0;
}
