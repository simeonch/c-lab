/***
FN:F79618
PID:3
GID:3
*/

#include <iostream>
#include <iomanip>
using namespace std;
string fnum, fg, result, fres;
double g;
string xform(string s, string f)
{
    if (fg == "A")
    g = 6.00;
    if (fg == "B")
    g = 5.00;
    if (fg == "C")
    g = 4.00;
    if (fg == "D")
    g = 3.00;
    if (fg == "E")
    g = 2.50;
    if (fg == "F")
    g = 2;
    result = fnum + "\t";
    return result;

}


int main()
{
    cout << "Enter faculty number and grade: " << endl;
    while (cin >> fnum >> fg)
    {
    string fres = xform(fnum, fg);
    cout << fres << setprecision(2) << fixed << g << endl;
    }
    return 0;
}
