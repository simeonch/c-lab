#include <iostream>
#include <cmath>
#include <string>
using namespace std;

int modi(string s)
{
    int res = 0;
    for(int i=s.size()-1;i >= 0;i--)
    {
        if(s[i] == '1')
        res += pow(2, (double)i);
    }
    return res;
}
int main()
{
    string num;
    cin >> num;
    cout << modi(num) << endl;
    return 0;
}
