#include <iostream>
#include <cmath>
#include <string>
#include <sstream>

using namespace std;

int bina(string s)
{
    int res = 0;
    for(int i=s.size()-1;i >= 0;i--)
    {
        if(s[i] == '1')
        res += pow(2, (double)i);
    }
    return res;
}

int octa(string s)
{
    int n;
    istringstream(s) >> oct >> n;
    return n;
}

int hexa(string s)
{
    int n;
    istringstream(s) >> hex >> n;
    return n;
}


int main()
{
    string num, sys;
    cin >> num;
    cin >> sys;
    if (sys == "bin")
    cout << bina(num) << endl;
    if (sys == "oct")
    cout << octa(num) << endl;
    if (sys == "hex")
    cout << hexa(num) << endl;
    return 0;
}
