#include <iostream>
using namespace std;
int n;
void func(int k, int& ec, int& oc)
{
    ec = 0;
    oc = 0;
    while (k > 0)
    {
        int z = k%10;
        k /= 10;
        if(z%2)
            oc++;
        else
            ec++;
    }
}

int main()
{
    cin >> n;
    while(n--)
    {
        int k;
        cin >> k;
        int ec, oc;
        func(k, ec, oc);
        cout << oc << " " << ec << endl;
    }

    return 0;
}
