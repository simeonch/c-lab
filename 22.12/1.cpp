#include <iostream>
using namespace std;
int n;
void func(int k)
{
    int ec = 0, oc = 0;
    while (k > 0)
    {
        int z = k%10;
        k /= 10;
        if(z%2)
            oc++;
        else
            ec++;
    }
    cout << oc << " " << ec << endl;
}

int main()
{
    cin >> n;
    while(n--)
    {
        int k;
        cin >> k;
        func(k);
    }

    return 0;
}
