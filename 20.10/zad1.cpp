#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    cout << "Enter a number:" << endl;
    int number;
    cin >> number;
    cout << number << "^2 = " << pow(number, 2) << " " << number << "^3 = " << pow(number, 3) << " " << number << "^4 = " << pow(number, 4) << " " << number << "^5 = " << pow(number, 5) << endl;
    return 0;

}
