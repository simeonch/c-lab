#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double a;
    cout << "Enter a:" << endl;
    cin >> a;
    double b;
    cout << "Enter b:" << endl;
    cin >> b;
    cout << "a + b = " << a+b << endl;
    cout << endl;
    cout << "a - b = " << a-b << endl;
    cout << endl;
    cout << "a * b = " << a*b << endl;
    cout << endl;
    cout << "a / b = " << a/b << endl;
    return 0;
}
