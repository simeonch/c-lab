#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double number;
    cout << "Enter a number in meters:" << endl;
    cin >> number;
    cout << number << " m = " << number/1000 << " km" << endl;
    cout << endl;
    cout << number << " m = " << number*10 << " dm" << endl;
    cout << endl;
    cout << number << " m = " << number*100 << " cm" << endl;
    return 0;
}
