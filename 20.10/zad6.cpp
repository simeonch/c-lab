#include <iostream>

using namespace std;

int main()
{
    int tank;
    cout << "Enter a tank size (liters)" << endl;
    cin >> tank;
    double fefc;
    cout << "Enter fuel efficiency (km per liter)" << endl;
    cin >> fefc;
    double price;
    cout << "Enter price per liter" << endl;
    cin >> price;
    cout << "You can travel " << tank*fefc << " km." << endl;
    cout << "For every 100 km it will cost " << price*fefc << " leva." << endl;
    return 0;
}
