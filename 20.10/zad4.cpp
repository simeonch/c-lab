#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double price;
    cout << "Enter price of something (leva);" << endl;
    cin >> price;
    double money;
    cout << "Enter your money (leva):" << endl;
    cin >> money;
    cout << "Your change is " << money-price << " leva" << " (" << (money-price)*100 << " stotinki)" << endl;
    return 0;
}
