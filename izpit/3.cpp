/***
FN:F79618
PID:3
GID:4
*/

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

using namespace std;

vector<double> v1;

int main()
{
    int n;
    string comm;
    cin >> n;
    for(int i = 0; i < n ; i++)
    {
        double el;
        cin >> el;
        v1.push_back(el);
    }
    while(cin >> comm)
    {
        if(comm == "insert")
        {
            int place;
            double numb;
            cin >> place;
            cin >> numb;
            v1.insert(v1.begin()+place, numb);
        }
        if(comm == "delete")
        {
            int delplace;
            cin >> delplace;
            v1.erase(v1.begin()+delplace);

        }
        if(comm == "shift")
        {
            int cnt;
            cin >> cnt;
            for(int k = 0; k < cnt; k++)
            {
                cout << v1[k] << " ";
            }
            cout << endl;
            for(int i = 0; i < cnt; i++)
            {
                v1.erase(v1.begin()+0);
            }
        }
        if(comm == "ssd")
        {
            int sum;
            for(int i = 0; i < v1.size(); i++)
            {
                if(v1[i] < 10)
                sum += v1[i];
                else
                {
                int d = fmod(v1[i],+10);
                sum += d;
                }
            }
            cout << sum << endl;

        }
        if(comm == "print")
        {
            for(int i = 0; i < v1.size(); i++)
            {
                cout << v1[i] << " " ;
            }
        cout << endl;
        }
    }

	return 0;
}
