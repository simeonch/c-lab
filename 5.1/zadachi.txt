1. ������� ����������: �������� �������� �� ������� �������������� ����� ���, ���� � ����� (BGN, EUR, USD), 
����� ���� �� ����������� ���� ������� � ����������� ��������� � �������� ����������� ��������. ���������� 
������ �� ��������� �������� �������:

�) �������� �� ���� (�� ��������� ����)

set BGN USD 1.5

�) ������������� �� ���� ������ � �����

convert USD EUR 12.45

��� ��� ������� ���� �����-����, ���������� ������ �� �������� �������� �������� �� 12.45 ������ �� ��� ��� 
� 2 ����� ���� ���������.

��� ���� ������� ����, �������� �� ��� ��� "N.A." ��� ���������.

�) ������ �� �����

rate BGN USD

��� ��� ������� ����, �������� �� �� ��� ��� � 6 ����� ���� ���������.

��� ���� ������� ����, �������� �� ��� ��� "N.A." ��� ���������.

��.

set BGN USD 1.5
rate BGN USD
1.5
rate USD BGN
0,666667
convert BGN USD 20
30.00
convert BGN EUR 20
N.A.
convert USD BGN 30
25.00

 

2. ������ �����: �������� ��������, ����� ���� (�� ���� �� ����������� ����) ���������� ����� � �� ��������� 
�� ����������� ����� � ������ �����. 

��.

����:

5
8
15

�����:

V
VIII
XV