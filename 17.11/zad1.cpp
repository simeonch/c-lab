/**
FN:f79618
PID:1
GID:1
*/
/**pri po-slojni funkcii ne moje napravo return */

#include <iostream>
using namespace std;

double a, b;
char ch;

double sum(double a, double b)
{
    double result = a + b;
    return result;
}

double dif(double a, double b)
{
    return a-b;
}

double mul(double a, double b)
{
    return a*b;
}
double div(double a, double b)
{
    return a/b;
}

void read()
{
    cin>>a>>b>>ch;
}
void solve()
{
    if (ch == '+')
        cout << sum(a,b) << endl;
    if (ch == '-')
        cout << dif(a,b) << endl;
    if (ch == '*')
        cout << mul(a,b) << endl;
    if (ch == '/')
        cout << div(a,b) << endl;
}

int main()
{
    for(int i=0;i<10;i++)
    {
    read();
    solve();
    }
    return 0;
}
