/***
FN:F00000
PID:2
GID:1
*/

#include <iostream>

using namespace std;

//functions here

double percentage_to_fraction(double percentage)
{
	return percentage / 100;
}

double compute_simple_interest(double deposit, double interest, int months_count)
{
	return deposit + deposit * interest * months_count;
}

double compute_advanced_interest(double deposit, double interest, int months_count)
{
	double result = deposit;
	for (int i = 0; i < months_count; i++)
	{
		result += result*interest;
	}
	return result;
}

int main()
{
	double deposit, interest_percentage;
	int months_count;

	cin >> deposit >> interest_percentage >> months_count;

	double interest = percentage_to_fraction(interest_percentage);

	double simple = compute_simple_interest(deposit, interest, months_count);
	double advanced = compute_advanced_interest(deposit, interest, months_count);

	cout << simple << endl;
	cout << advanced << endl;

	return 0;
}