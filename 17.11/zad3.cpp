/**
FN:f79618
PID:3
GID:1
*/
#include <iostream>
#include <string>
using namespace std;
string in;

bool is_number(string s)
{
    for(int i=0;i < s.length();i++)
    {
        //bool ok = isdigit(s[i]);
        //if(!ok)
        //return false;
        if(!isdigit(s[i]))
            return false;
    }
    return true;
}
string modify(string s)
{
    string res = "";
    if(is_number(s))
        res = s.append("[OK]");
    else
        res = s.append("[FAIL]");
    return res;
}
void read()
{
    cin >> in;
}
int main()
{
    read();
    cout << modify(in) << endl;
    return 0;
}
