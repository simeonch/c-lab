/***
FN:F00000
PID:3
GID:1
*/

#include <iostream>
#include <cctype>

using namespace std;

bool is_number(const string &str)
{
	for (int i = 0; i < str.length(); i++)
	{
		if (!isdigit(str[i]))
			return false;
	}

	return true;
}

void validate(string &str)
{
	if (is_number(str))
		str.append(" [OK]");
	else
		str.append(" [FAIL]");
}

int main()
{
	string input;
	
	cin >> input;

	validate(input);

	cout << input;

	return 0;
}