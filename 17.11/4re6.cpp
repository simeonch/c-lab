/***
FN:F00000
PID:4
GID:1
*/

#include <iostream>
#include <iomanip>

using namespace std;

void print_headers()
{
	cout << setw(10) << left << "Faculty No" << right;
	cout << setw(6) << "G1";
	cout << setw(6) << "G2";
	cout << setw(6) << "G3";
	cout << setw(6) << "G4";
	cout << endl;
}
void print_row(string fn, double grade1, double grade2, double grade3, double grade4)
{
	//setup precision 2 digits after the decimal point
	cout << fixed << setprecision(2);

	cout << setw(10) << left << fn << right;
	cout << setw(6) << grade1;
	cout << setw(6) << grade2;
	cout << setw(6) << grade3;
	cout << setw(6) << grade4;

	cout << endl;

}

int main()
{
	string facultyNumber;
	double grade1, grade2, grade3, grade4;

	print_headers();
	while (cin >> facultyNumber >> grade1 >> grade2 >> grade3 >> grade4)
	{
		print_row(facultyNumber, grade1, grade2, grade3, grade4);
	}

	return 0;
}