/***
FN:F00000
PID:1
GID:1
*/

#include <iostream>

using namespace std;

double add(double a, double b)
{
	return a + b;
}
double substract(double a, double b)
{
	return a - b;
}
double multiply(double a, double b)
{
	return a * b;
}
double divide(double a, double b)
{
	return a / b;
}

double do_the_math(double a, double b, char op)
{
	double result;
	if (op == '+')
	{
		result = add(a, b);
	}
	else if (op == '-')
	{
		result = substract(a, b);
	}
	else if (op == '*')
	{
		result = multiply(a, b);
	}
	else if (op == '/')
	{
		result = divide(a, b);
	}
	else
		throw 1; //invalid operation, we can catch this errror and inspect it in the main function.
	return result;
};


//this is the same as above, but using a switch instead of series of if-elseif
double do_the_math_with_switch(double a, double b, char op)
{
	double result;
	switch (op)
	{
	case '+':
		result = add(a, b);
		break;
	case '-':
		result = substract(a, b);
		break;
	case '*':
		result = multiply(a, b);
		break;
	case '/':
		result = divide(a, b);
		break;
	default:
		throw 1; //invalid operation, we can catch this errror and inspect it in the main function.
	}
	return result;
};

int main()
{
	double operand1, operand2;
	char operation;

	cin >> operand1 >> operand2 >> operation;

	double result = do_the_math(operand1, operand2, operation);

	cout << result << endl;

	return 0;
}