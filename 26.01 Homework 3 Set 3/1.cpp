/***
FN:F79618
PID:1
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<double> v1;
vector<double> v2;

double happy(vector<double> v1, vector<double> v2)
{
    double res = 0;
    double maxi = min( v1.size(), v2.size() );
    for(int i = 0; i < maxi; i++)
    {
        res += v1[i]/v2[i];
    }
    return res;
}
int main()
{
    int n;
    cin >> n;
    for(int i = 0; i < n; i++)
    {
        double el1;
        cin >> el1;
        v1.push_back(el1);
    }
    for(int z = 0; z < n; z++)
    {
        double el2;
        cin >> el2;
        v2.push_back(el2);
    }
    cout << happy(v1,v2) << endl;

	return 0;
}
