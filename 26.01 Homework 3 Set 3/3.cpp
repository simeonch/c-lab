/***
FN:F79618
PID:3
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<double> v1;

vector<double> shift(vector<double>& v1)
{
    int s = v1.size()-1;
    v1.insert(v1.begin()+s,v1[0]);
    v1.erase(v1.begin());
    return v1;
}

vector<double> rev(vector<double>& v1)
{
    int r = 0;
    for(int u = v1.size()-1; u >= 0; u--)
    {
        int m = v1.size();
        v1.insert(v1.begin()+r,v1[m]);
        v1.pop_back();
        r++;
    }
    return v1;
}

int main()
{
    int n;
    string comm;
    cin >> n;
    for(int i = 0; i < n ; i++)
    {
        double el;
        cin >> el;
        v1.push_back(el);
    }
    while(cin >> comm)
    {
        if(comm == "add")
        {
            int place;
            double nadd;
            cin >> place;
            cin >> nadd;
            v1.insert(v1.begin()+place, nadd);
        }
        if(comm == "remove")
        {
            int dplace;
            cin >> dplace;
            v1.erase(v1.begin()+dplace);

        }
        if(comm == "shift_left")
        {
            shift(v1);
        }
        if(comm == "reverse")
        {
            rev(v1);
        }
        if(comm == "print")
        {
            for(int i = 0; i < v1.size(); i++)
            {
                cout << v1[i] << endl;
            }

        }
        if(comm == "exit")
        {
            break;
        }
    }

	return 0;
}
