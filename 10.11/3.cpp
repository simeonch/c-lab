#include <iostream>
#include <cmath>
using namespace std;
int main()
{
    double sum, rate, balance;
    int years;
    cout << "Enter the sum of the deposit: " << endl;
    cin >> sum;
    cout << "Enter the yearly interest rate in %: " << endl;
    cin >> rate;
    cout << "Enter years: " << endl;
    cin >> years;
    balance = sum*pow(1+rate/100, years);
    cout << "After " << years << " years, the balance will be " << balance << endl;
    return 0;
}
