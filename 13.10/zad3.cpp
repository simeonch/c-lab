#include <iostream>

using namespace std;

int main()
{
    string fname;
    cout << "Enter your first name:" << endl;
    cin >> fname;
    string lname;
    cout << "Enter your last name:" << endl;
    cin >> lname;
    string address;
    cout << "Enter your address:" << endl;
    cin.ignore();
    getline(cin, address);
    cout << endl;
    cout << "===============" << endl;
    cout << endl;
    cout << fname << endl;
    cout << endl;
    cout << lname << endl;
    cout << endl;
    cout << address << endl;
    cout << endl;
    cout << "===============" << endl;
    return 0;
}
