#include <iostream>

using namespace std;

int main()
{
    cout << "Enter your name:" << endl;
    string name;
    cin >> name;
    cout << endl;
    cout << "===============";
    cout << endl;
    cout << "=    " << name << "     =" << endl;
    cout << endl;
    cout << "===============" << endl;
    return 0;
}
