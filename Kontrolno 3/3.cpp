/***
FN:F79618
PID:3
GID:4
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<double> v1;

vector<double> update(vector<double>& v1)
{
    return v1;
}

int main()
{
    int n;
    string comm;
    cin >> n;
    for(int i = 0; i < n ; i++)
    {
        double el;
        cin >> el;
        v1.push_back(el);
    }
    while(cin >> comm)
    {
        if(comm == "insert")
        {
            int place;
            double numb;
            cin >> place;
            cin >> numb;
            v1.insert(v1.begin()+place, numb);
        }
        if(comm == "delete")
        {
            int delplace;
            cin >> delplace;
            v1.erase(v1.begin()+delplace);

        }
        if(comm == "update_range")
        {
            int sidx, eidx, nmb;
            cin >> sidx >> eidx >> nmb;
            if(sidx>eidx)
            {
            for(int i = sidx; i <= eidx; i++)
            v1[i] += nmb;
            }
            if(eidx>sidx)
            {
            for(int i = eidx; i >= sidx; i--)
            v1[i] += nmb;
            }
        }
        if(comm == "print")
        {
            for(int i = 0; i < v1.size(); i++)
            {
                cout << v1[i] << " " ;
            }
        cout << endl;
        }
        if(comm == "exit")
        {
            break;
        }
    }

	return 0;
}
