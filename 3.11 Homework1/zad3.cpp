        #include <iostream>
using namespace std;
int main()
{
    double op1,op2;
    string operation;
    cout << "Enter operation (+ / - / x / : ):" << endl;
    cin >> operation;
    cout << "Enter operand 1:" << endl;
    cin >> op1;
    cout << "Enter operand 2:" << endl;
    cin >> op2;
    if (operation == "+")
    {
        cout << op1 << " + " << op2 << " = " << op1+op2 << endl;
    }
    else if (operation == "-")
    {
        cout << op1 << " - " << op2 << " = " << op1-op2 << endl;
    }
    else if (operation == "x" || operation == "*")
    {
        cout << op1 << " x " << op2 << " = " << op1*op2 << endl;
    }
    else if (operation == ":" || operation == "/")
    {
        cout << op1 << " : " << op2 << " = " << op1/op2 << endl;
    }
    return 0;
}
