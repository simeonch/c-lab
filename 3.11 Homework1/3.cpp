#include <iostream>
using namespace std;
int main()
{
    string name;
    cout << "Enter employee:" << endl;
    cin >> name;
    double wage;
    cout << "Enter his hourly wage:" << endl;
    cin >> wage;
        if(wage<0)
    {
        cout << "You can't have negative salary!" << endl;
        return 0;
    }
    double hours;
    cout << "Enter how many hours the employee has worked last week:" << endl;
    cin >> hours;
    int result = wage*hours;
    if(hours>168)
    {
        cout << "You can't work more than 168 hours in a week!" << endl;
    }
    if(hours<0)
    {
        cout << "You can't work negative hours!" << endl;
    }
    if(wage>0 && hours<168)
    {
           cout << "The salary of " << name << " for the last week is " << result << " BGN." << endl;
    }
    return 0;
}
