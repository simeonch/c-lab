#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    double share;
    cout << "Enter your ownership in KTB:" << endl;
    cin >> share;
    long int result = (share/100)*58000000;
    if(share>100)
    {
        cout << "You can't own more than 100%" << endl;
    }
    if(share<0)
    {
        cout << "You can't own less than 0%" << endl;
    }
    if(share>=0 && share<=100)
    {
        cout << fixed << setprecision(2);
        cout << "You have " << share/100 << " shares or " << result << " BGN." << endl;
    }
    return 0;
}
