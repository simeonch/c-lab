#include <iostream>
using namespace std;
int main()
{
    double g1,g2;
    cout << "Enter grade 1: " << endl;
    cin >> g1;
    cout << "Enter grade 2: " << endl;
    cin >> g2;
    if (g1>=2 && g1<=6 && g2>=2 && g2<=6)
    {
        cout << "Your average grade is " << (g1+g2)/2 << endl;
    }
    if(g1<2 || g1>6)
    {
        cout << "Grade 1 is not valid in Bulgaria." << endl;
    }
    if(g2<2 || g2>6)
    {
        cout << "Grade 2 is not valid in Bulgaria." << endl;
    }
    return 0;
}
