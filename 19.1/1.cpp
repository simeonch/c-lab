#include <iostream>
#include <string>
#include <ctype.h>

using namespace std;

bool areq(string const &a, string const &b)
{
    if(a.size() != b.size())
        return false;
    for(int i = 0; i < a.size();i++)
        if(tolower(a[i]) != tolower(b[i]))
        return false;
    return true;
}

int main()
{
    string a, b;
    while (cin >> a >> b)
    {
        cout << a << "-" << b << " : " << areq(a, b) << endl;
    }
    return 0;
}
