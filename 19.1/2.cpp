#include <iostream>
#include <string>
#include <ctype.h>

using namespace std;

string alphabet = " ABCDEFGHIJKLMNOPQRSTUWXYZ";
string myTolower(string s)
{
    for(int i = 0; i < s.size();i++)
        s[i] = tolower (s[i]);
    return s;
}
string modi(string const key)
{
    string copy = myTolower(alphabet);
    //key
    for(int i = 0; i < key.size();i++)
    {
        int idx = -1;
        //alphabet
        for (int j = 0; j < copy.size();j++)
            if(key[i] == copy[j])
            {
                idx = j;
                break;
            }
            if(idx == -1)
                continue;

        copy = copy.erase(idx, 1);
    }
    return key + copy;
}

string encrypt(string modifiedAlphabet, string input)
{
    string res = "";
    for(int i = 0;i < input.size();i++)
        if(isalpha(input[i]))
        {
            //a->z .. A->Z...
            int idx = input[i]-97;
            res += modifiedAlphabet[idx];
        }
        else
        {
            //.,4125--=-
            res += input[i];
        }
    return res;
}

string decrypt(string modifiedAlphabet, string input)
{
    string res = "";
    for(int i = 0;i < input.size();i++)
        if(isalpha(input[i]))
        {
            //a->z .. A->Z...
            int idx = input[i]+97;
            res += modifiedAlphabet[idx];
        }
        else
        {
            //.,4125--=-
            res += input[i];
        }
    return res;
}
int main()
{
    string key;
    cin >> key;
    key = myTolower(key);
    string modified = modi(key);
    string s, f;
    while(getline(cin, s))
        cout << encrypt(modified, s) << endl;
    while(getline(cin, f))
        cout << decrypt(modified, f) << endl;
    return 0;
}
