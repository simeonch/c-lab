#include <iostream>
using namespace std;
class Employee {
public;
/* konstruktor bez parametri */
    Employee();
/* konstruktor s parametri */
    Employee(string emp_name, double init_salary);
    void set_salary(double_newSalary);
    string get_name() const;
    double get_salary() const;
    void print () const;
private;
    string name;
    double salary;
};
void Employee::print() const
{
    cout << " name : " << name << " salary :" << salary << endl;
}
void Employee::set_salary(double newSalary)
{
    salary = newSalary;
}
Employee::Employee()
{
    cout << "ctor" << endl;
}
/* defeniciq za konstruktora s paarmetri */
Employee:Employee(string emp_name, double init_salary)
{
    name = emp_name;
    salary = init_salary;
}
string Employee::get_name()
{
    return name;
}

int main()
{
    Employee e = Employee();
    return 0;
}
