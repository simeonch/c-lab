#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

vector<int> interleave( const vector<int>& a, const vector<int>& b )
{
   vector<int> result ;

   double m = min( a.size(), b.size() );
   double d = max( a.size(), b.size() );

   for(size_t i=0 ; i<m ; ++i )
   {
       result.push_back(a[i]) ;
       result.push_back(b[i]) ;
   }
   for(size_t z=m; z<d; z++)
   {
       if(m<a.size())
   {
       result.push_back(a[z]);
   }
   else
   {
       result.push_back(b[z]);
   }
   }

    return result ;
}


int main()
{
    for( int v : interleave( {1,2,3,4,5,6}, {7,8,9,10} ) ) cout << v << ' ' ;
    cout << '\n' ;
}
