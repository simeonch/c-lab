#include <iostream>
#include <string>
using namespace std;

int main()
{
    string book;
    getline(cin, book);
    string f1;
    cin >> f1;
    int index = book.find(f1);
    while (index != -1)
    {
        cout << "Found at: " << index << endl;
        index = book.find(f1, index + 1);
    }
    return 0;
}
