#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

vector<int> interleave( const vector<int>& a, const vector<int>& b )
{
   vector<int> result ;

   double m = min( a.size(), b.size() ) ;

   for(size_t i=0 ; i<m ; ++i )
   {
       result.push_back(a[i]) ;
       result.push_back(b[i]) ;
   }
   if( m < a.size() ) result.insert( result.end(), a.begin()+m, a.end() ) ;
   if( m < b.size() ) result.insert( result.end(), b.begin()+m, b.end() ) ;

   return result ;
}


int main()
{
    for( int v : interleave( {1,2,3,4,5,6}, {7,8,9,10} ) ) cout << v << ' ' ;
    cout << '\n' ;
}
