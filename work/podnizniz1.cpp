#include <iostream>
#include <string>
using namespace std;

int main()
{
    string book = "I really wanna do this. But how do I do this? Can you help me do this?";
    int index = book.find("this");
    while (index != -1)
    {
        cout << "Found at: " << index << endl;
        index = book.find("this", index + 1);
    }
    return 0;
}
